// M_matrix.h

#pragma once
#include "matrix.h"

#pragma managed
#using <mscorlib.dll>

namespace M_Matrix {
	public ref class M_matrix : public System::ICloneable
	{
	private:
		Matrix::matrix<double> * _matrix;
	public:
		M_matrix();
		M_matrix(System::Int32 width, System::Int32 height);
		M_matrix(System::Int32 width, System::Int32 height, array<System::Double>^ arrprt);
		M_matrix(const Matrix::matrix<double> &m);
		M_matrix(const M_matrix %m);
		virtual Object^ Clone();
		~M_matrix();
		virtual M_matrix^ Assign(M_matrix^ otherOne);
		//Member Functions
		System::Double At(System::Int32 x, System::Int32 y);
		property System::Int32 Width {
			System::Int32 get() {
				return System::Convert::ToInt32(_matrix->getWidth());
			}
		}
		property System::Int32 Height {
			System::Int32 get() {
				return System::Convert::ToInt32(_matrix->getHeight());
			}
		}
		//friend functions
		System::Double determinant(System::Int32 col);
		System::Double determinant2x2();
		void adjoint();
		void cofactor();
		void transpose();
		void invert2x2();
		void invert();
		//operators
		void add(const M_matrix %m);
		void subtract(const M_matrix %m);
		void negative();
		void multiply(System::Double b);
		void multiply(const M_matrix %m);
		System::Boolean isEqual(const M_matrix %m);
		System::Boolean isNotEqual(const M_matrix %m);
		System::String^ toString();
	};
}