// This is the main DLL file.

#include "stdafx.h"
#include "MatrixManaged.h"

namespace M_Matrix{
	System::Object^ M_matrix::Clone(){
		M_matrix ^output = gcnew M_matrix();
		*(output->_matrix) = *_matrix;
		return output;
	}

	M_matrix::M_matrix(){
		_matrix = new Matrix::matrix<double>();
	}

	M_matrix::M_matrix(System::Int32 width, System::Int32 height){
		int w = (int)width;
		int h = (int)height;
		_matrix = new Matrix::matrix<double>(w, h);
	}

	M_matrix::M_matrix(System::Int32 width, System::Int32 height, array<System::Double>^ arrptr){
		int w = (int)width;
		int h = (int)height;
		double * data = NULL;
		try {
			data = new double[w * h];
		}
		catch (std::bad_alloc){
			throw Matrix::matrixException(Matrix::MEMORY_ERROR);
		}
		//convert
		for (int i = 0; i < arrptr->Length; i++){
			data[i] = (double)arrptr[i];
		}
		_matrix = new Matrix::matrix<double>(w, h, data);
	}

	M_matrix::M_matrix(const M_matrix %m){
		_matrix = new Matrix::matrix<double>(*(m._matrix));
	}

	M_matrix::M_matrix(const Matrix::matrix<double> &m){
		_matrix = new Matrix::matrix<double>(m);
	}

	M_matrix::~M_matrix(){
		_matrix->~matrix<double>();
	}

	System::Double M_matrix::At(System::Int32 x, System::Int32 y){
		return _matrix->operator[](y)[x];
	}

	M_matrix^ M_matrix::Assign(M_matrix^ otherOne){
		if (otherOne != this){
			*_matrix = *(otherOne->_matrix);
		}
		return this;
	}

	System::Double M_matrix::determinant2x2(){
		return System::Convert::ToDouble(Matrix::determinant2x2(*_matrix));
	}

	System::Double M_matrix::determinant(System::Int32 col){
		return System::Convert::ToDouble(Matrix::determinant(*_matrix, col));
	}

	void M_matrix::adjoint(){
		*_matrix = Matrix::adjoint(*_matrix);
	}

	void M_matrix::cofactor(){
		*_matrix = Matrix::cofactor(*_matrix);
	}

	void M_matrix::transpose(){
		*_matrix = Matrix::transpose(*_matrix);
	}

	void M_matrix::invert2x2(){
		*_matrix = Matrix::invert2x2(*_matrix);
	}

	void M_matrix::invert(){
		*_matrix = Matrix::invert(*_matrix);
	}

	void M_matrix::add(const M_matrix %m){
		*_matrix = ((*_matrix) + *(m._matrix));
	}

	void M_matrix::subtract(const M_matrix %m){
		*_matrix = ((*_matrix) - *(m._matrix));
	}

	void M_matrix::negative(){
		*_matrix = -(*_matrix);
	}

	void M_matrix::multiply(System::Double b){
		double k = static_cast<double>(b);
		*_matrix = (*_matrix) * k;
	}

	void M_matrix::multiply(const M_matrix %m){
		*_matrix = (*_matrix) * *(m._matrix);
	}
	
	System::Boolean M_matrix::isEqual(const M_matrix %m){
		return System::Convert::ToBoolean(*_matrix == *(m._matrix));
	}

	System::Boolean M_matrix::isNotEqual(const M_matrix %m){
		return System::Convert::ToBoolean(*_matrix != *(m._matrix));
	}

	System::String^ M_matrix::toString(){
		return gcnew System::String(Matrix::toString(*_matrix).c_str());
	}
}