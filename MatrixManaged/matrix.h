#ifndef MATRIX_H
#define MATRIX_H

#include <string>
#include <iostream>
#include "matrixError.h"

namespace Matrix{

	template <class Type>
	class matrix;

	template <class >Type
	class vector;

	template <class Type> Type determinant(const matrix<Type> &a, int row);
	template <class Type> Type determinant2x2(const matrix<Type> &a);
	template <class Type> matrix<Type> adjoint(const matrix<Type> &a);
	template <class Type> matrix<Type> cofactor(const matrix<Type> &a);
	template <class Type> matrix<Type> transpose(const matrix<Type> &a);
	template <class Type> matrix<double> invert2x2(const matrix<Type> &a);
	template <class Type> matrix<double> invert(const matrix<Type> &a);
	template <class Type> matrix<Type> operator+(const matrix<Type> &a, const matrix<Type> &b);
	template <class Type> matrix<Type> operator-(const matrix<Type> &a);
	template <class Type> matrix<Type> operator-(const matrix<Type> &a, const matrix<Type> &b);
	template <class Type> vector<Type> operator*(const matrix<Type> &a, const vector<Type> &b);
	template <class Type> matrix<Type> operator*(const matrix<Type> &a, Type b);
	template <class Type> matrix<Type> operator*(const matrix<Type> &a, const matrix<Type> &b);
	template <class Type> bool operator==(const matrix<Type> &a, const matrix<Type> &b);
	template <class Type> bool operator!=(const matrix<Type> &a, const matrix<Type> &b);
	template <class Type> std::ostream& operator<<(std::ostream &out, const matrix<Type> &a);
	template <class Type> std::string toString(const matrix<Type> &m);

	//template <typename Typeout>
	template <class Type>
	class matrix{
	protected:
		int size;
		int width;
		int height;
		Type* data;
	public:
		matrix(int in_width, int in_height);
		matrix() : data(NULL), width(0), height(0), size(0) {}
		matrix(const matrix<Type> &in_matrix);
		matrix(int in_width, int in_height, Type* input);
		~matrix(){ delete[] data; data = NULL; };
		int getWidth()const{ return width; };
		int getHeight()const{ return height; };
		Type* getRow(int y)const;
		Type* getColum(int x)const;
		void map(Type(*function)(Type));
		//Type conversion
		operator matrix<bool>();
		operator matrix<unsigned char>();
		operator matrix<short>();
		operator matrix<int>();
		operator matrix<float>();
		operator matrix<double>();
		//Matrix manipulation
		friend Type determinant <>(const matrix<Type> &a, int col);
		friend Type determinant2x2 <>(const matrix<Type> &a);
		friend matrix<Type> adjoint <>(const matrix<Type> &a);
		friend matrix<Type> cofactor <>(const matrix<Type> &a);
		friend matrix<Type> transpose <>(const matrix<Type> &a);
		friend matrix<double> invert2x2 <>(const matrix<Type> &a);
		friend matrix<double> invert <>(const matrix<Type> &a);
		matrix<Type>& operator=(const matrix<Type> &a);
		Type* operator[](int a)const;
		friend matrix<Type> operator+ <>(const matrix<Type> &a, const matrix<Type> &b);
		friend matrix<Type> operator- <>(const matrix<Type> &a);
		friend matrix<Type> operator- <>(const matrix<Type> &a, const matrix<Type> &b);
		friend vector<Type> operator* <>(const matrix<Type> &a, const vector<Type> &b);
		friend matrix<Type> operator* <>(const matrix<Type> &a, Type b);
		friend matrix<Type> operator* <>(const matrix<Type> &a, const matrix<Type> &b);
		friend bool operator==<>(const matrix<Type> &a, const matrix<Type> &b);
		friend bool operator!=<>(const matrix<Type> &a, const matrix<Type> &b);
		friend std::ostream& operator<< <>(std::ostream &out, const matrix<Type> &a);
		friend std::string toString <>(const matrix<Type> &m);
	};

	template <class Type>
	class vector : public matrix < Type > {
	public:
		vector(){ data = NULL; };
		vector(int in_height, Type * in_data);
		vector(int in_height);
	};

	template <class Type>
	matrix<Type> operator-(const matrix<Type> &a){
		return a * static_cast<Type>(-1);
	}

	template <class Type>
	matrix<Type> operator-(const matrix<Type> &a, const matrix<Type> &b){
		return a + (-b);
	}

	template <class Type>
	bool operator!=(const matrix<Type> &a, const matrix<Type> &b){
		return !(a == b);
	}

	template <class Type>
	matrix<Type>::matrix(int in_width, int in_height, Type* input){
		static_assert(std::_Is_numeric<Type>::value,
			"Cannot initialise matrix with non numeric types");
		width = in_width;
		height = in_height;
		size = (width > height) ? width : height;
		try {
			data = new Type[size*size];
		}
		catch (std::bad_alloc){
			throw(matrixException(MEMORY_ERROR));
		}
		int i = 0;
		for (int y = 0; y < height; y++){
			for (int x = 0; x < width; x++){
				data[y*size + x] = input[i++];
			}
		}
	}

	template <class Type>
	matrix<Type>::matrix(int in_width, int in_height){
		static_assert(std::_Is_numeric<Type>::value,
			"Cannot initialise matrix with non numeric types");
		width = in_width;
		height = in_height;
		size = (width > height) ? width : height;
		try {
			data = new Type[size*size];
		}
		catch (std::bad_alloc){
			throw(matrixException(MEMORY_ERROR));
		}
	}

	template <class Type>
	matrix<Type>::matrix(const matrix<Type> &in_matrix){
		static_assert(std::_Is_numeric<Type>::value,
			"Cannot initialise matrix with non numeric types");
		if (in_matrix.data){
			width = in_matrix.width;
			height = in_matrix.height;
			size = in_matrix.size;
			try {
				data = new Type[size*size];
			}
			catch (std::bad_alloc){
				throw(matrixException(MEMORY_ERROR));
			}
			for (int y = 0; y < height; y++){
				for (int x = 0; x < width; x++){
					data[y*size + x] = in_matrix[y][x];
				}
			}
		}
		else {
			data = NULL;
		}
	}

	template <class Type>
	Type* matrix<Type>::getColum(int x)const{
		Type * output = NULL;
		try {
			output = new Type[height];
		}
		catch (std::bad_alloc){
			throw(matrixException(MEMORY_ERROR));
		}
		for (int y = 0; y < height; y++){
			output[y] = data[x + y*size];
		}
		return output;
	}

	template <class Type>
	Type* matrix<Type>::getRow(int y)const{
		Type * output = NULL;
		try {
			output = new Type[width];
		}
		catch (std::bad_alloc){
			throw(matrixException(MEMORY_ERROR));
		}
		int start = y*size;
		for (int x = 0; x < width; x++){
			output[x] = data[start++];
		}
		return output;
	}

	template <class Type>
	void matrix<Type>::map(Type(*function)(Type)){
		for (int y = 0; y < height; y++){
			for (int x = 0; x < width; x++){
				data[y*size + x] = function(data[y*size + x]);
			}
		}
	}

	template <class Type>
	matrix<double> invert2x2(const matrix<Type> &a){
		matrix<double> output(2, 2);
		double det = static_cast<double>(determinant2x2(a));
		if (!det)
			throw matrixException(MATH_ERROR);
		double d = 1.0 / det;
		output[0][0] = (a[1][1] * d);
		output[0][1] = (-a[0][1] * d);
		output[1][0] = (-a[1][0] * d);
		output[1][1] = (a[0][0] * d);
		return output;
	}

	template <class Type>
	matrix<double> invert(const matrix<Type> &a){
		int w = a.width;
		int h = a.height;
		if (w != h)
			throw matrixException(DIMENSION_ERROR);
		if (h == 2){ return invert2x2(a); }
		matrix<double> output(w, h);
		double det = static_cast<double>(determinant(a));
		if (!det)
			throw matrixException(MATH_ERROR);
		return static_cast<matrix<double>>(adjoint(a)) * (1.0 / det);
	}

	template <class Type>
	matrix<Type> adjoint(const matrix<Type> &a){
		return transpose(cofactor(a));
	}

	template <class Type>
	Type determinant(const matrix<Type> &a, int row = 0){
		int w = a.width;
		int h = a.height;
		if (h != w)
			throw matrixException(DIMENSION_ERROR);
		Type output = 0;
		bool s = (row == 0) || (row % 2 == 0);
		if (h == 2){
			return (determinant2x2(a));
		}
		else{
			for (int tmpX = 0; tmpX < w; tmpX++){
				int ycount = 0;
				matrix<Type> tmp(w - 1, h - 1);
				for (int y = 0; y < h; y++){
					if (y != row){
						int xcount = 0;
						for (int x = 0; x < w; x++){
							if (x != tmpX){
								tmp[ycount][xcount++] = a[y][x];
							}
						}
						ycount++;
					}
				}
				output += s ? ((determinant(tmp) * (a[row][tmpX]))) : -((determinant(tmp) * (a[row][tmpX])));
				s = !s;
			}
		}
		return output;
	}

	template <class Type>
	Type determinant2x2(const matrix<Type> &a){
		return a[0][0] * a[1][1] - a[0][1] * a[1][0];
	}

	template <class Type>
	matrix<Type> cofactor(const matrix<Type> &a){
		int w = a.width;
		int h = a.height;
		if (h != w)
			throw matrixException(DIMENSION_ERROR);
		matrix<Type> output(w, h);
		for (int y = 0; y < h; y++){
			for (int x = 0; x < w; x++){
				matrix<Type> det(w - 1, h - 1);
				int ycount = 0;
				for (int ydet = 0; ydet < h; ydet++){
					int xcount = 0;
					if (ydet != y){
						for (int xdet = 0; xdet < w; xdet++){
							if (xdet != x){
								det[ycount][xcount++] = a[ydet][xdet];
							}
						}
						ycount++;
					}
				}
				if ((y + x) % 2 == 0) {
					output[y][x] = determinant(det);
				}
				else {
					output[y][x] = -determinant(det);
				}
			}
		}
		return output;
	}

	template <class Type>
	matrix<Type> transpose(const matrix<Type> &a){
		matrix<Type> output(a);
		for (int y = 0; y < a.size; y++){
			for (int x = 0; x < y; x++){
				Type tmp = output.data[y*output.size + x];
				output.data[y*output.size + x] = output.data[x*output.size + y];
				output.data[x*output.size + y] = tmp;
			}
		}
		int tmp = output.width;
		output.width = output.height;
		output.height = tmp;
		return output;
	}

	template <class Type>
	matrix<Type> operator+(const matrix<Type> &a, const matrix<Type> &b){
		int a_width = a.width;
		int a_height = a.height;
		int b_width = b.width;
		int b_height = b.height;
		if (a_width != b_width || a_height != b_height)
			throw matrixException(DIMENSION_ERROR);
		int myWidth = a_width;
		int myHeight = a_height;
		matrix<Type> output(myWidth, myHeight);
		for (int y = 0; y < myHeight; y++){
			for (int x = 0; x < myWidth; x++){
				output[y][x] = a[y][x] + b[y][x];
			}
		}
		return output;
	}

	template <class Type>
	matrix<Type>& matrix<Type>::operator=(const matrix<Type> &a){
		if (this == &a){
			return *this;
		}
		delete[] data;

		width = a.width;
		height = a.height;
		size = (width > height) ? width : height;

		if (a.data){
			try {
				data = new Type[size*size];
			}
			catch (std::bad_alloc){
				throw(matrixException(MEMORY_ERROR));
			}
			for (int y = 0; y < height; y++){
				for (int x = 0; x < width; x++){
					data[y*size + x] = a[y][x];
				}
			}
		}
		else {
			data = NULL;
		}
		return *this;
	}

	template <class Type>
	Type* matrix<Type>::operator[](int a)const{
		if (a < 0 || a >= height)
			throw matrixException(BOUNDS_ERROR);
		return data + size * a;
	}

	template <class Type>
	matrix<Type> operator*(const matrix<Type> &a, const matrix<Type> &b){
		if (a.width != b.height || a.height != b.width)
			throw matrixException(DIMENSION_ERROR);

		matrix<Type> output(a.height, b.width);
		for (int y = 0; y < a.height; y++){
			Type * tmpRow = a.getRow(y);
			for (int x = 0; x < b.width; x++){
				Type * tmpCol = b.getColum(x);
				Type total = 0;
				for (int i = 0; i < a.width; i++){
					total += tmpRow[i] * tmpCol[i];
				}
				output[y][x] = total;
			}
		}
		return output;
	}

	template <class Type>
	vector<Type> operator*(const matrix<Type> &a, const vector<Type> &b){
		int m_height = a.height;
		int m_width = a.width;
		int v_height = b.height;
		if (m_width != v_height)
			throw matrixException(DIMENSION_ERROR);
		vector<Type> output(m_height);
		for (int y = 0; y < m_height; y++){
			Type total = 0;
			for (int x = 0; x < m_width; x++){
				total += a[y][x] * b[x][0];
			}
			*output[y] = total;
		}
		return output;
	}



	template <class Type>
	matrix<Type> operator*(const matrix<Type> &a, Type b){
		int height = a.height;
		int width = a.width;
		matrix<Type> output(width, height);
		for (int y = 0; y < a.height; y++){
			for (int x = 0; x < a.width; x++){
				output[y][x] = a[y][x] * b;
			}
		}
		return output;
	}

	template <class Type>
	bool operator==(const matrix<Type> &a, const matrix<Type> &b){
		if (a.height != b.height || a.width != b.width){
			return false;
		}
		int height = a.height, width = a.width;
		for (int y = 0; y < height; y++){
			for (int x = 0; x < width; x++){
				if (a[y][x] != b[y][x]){
					return false;
				}
			}
		}
		return true;
	}

	template <class Type>
	std::ostream& operator<<(std::ostream &out, const matrix<Type> &a){
		int height = a.height;
		int width = a.width;
		for (int y = 0; y < height; y++){
			for (int x = 0; x < width; x++){
				out << a[y][x] << ' ';
			}
			out << std::endl;
		}
		return out;
	}

	template <class Type>
	std::string toString(const matrix<Type> &m){
		std::string output;
		for (int y = 0; y < m.height; y++){
			for (int x = 0; x < m.width; x++){
				output.append(std::to_string(m[y][x]));
				output.append(",  ");
			}
			output.append("\n");
		}
		return output;
	}

	template <class Type>
	matrix<Type>::operator matrix<bool>(){
		matrix<bool> output(width, height);
		for (int y = 0; y < height; y++){
			for (int x = 0; x < width; x++){
				output[y][x] = static_cast<bool>(data[y*width + x]);
			}
		}
		return output;
	}

	template <class Type>
	matrix<Type>::operator matrix<unsigned char>(){
		matrix<unsigned char> output(width, height);
		for (int y = 0; y < height; y++){
			for (int x = 0; x < width; x++){
				output[y][x] = static_cast<unsigned char>(data[y*size + x]);
			}
		}
		return output;
	}

	template <class Type>
	matrix<Type>::operator matrix<short>(){
		matrix<short> output(width, height);
		for (int y = 0; y < height; y++){
			for (int x = 0; x < width; x++){
				output[y][x] = static_cast<short>(data[y*size + x]);
			}
		}
		return output;
	}

	template <class Type>
	matrix<Type>::operator matrix<int>(){
		matrix<int> output(width, height);
		for (int y = 0; y < height; y++){
			for (int x = 0; x < width; x++){
				output[y][x] = static_cast<int>(data[y*size + x]);
			}
		}
		return output;
	}

	template <class Type>
	matrix<Type>::operator matrix<float>(){
		matrix<float> output(width, height);
		for (int y = 0; y < height; y++){
			for (int x = 0; x < width; x++){
				output[y][x] = static_cast<float>(data[y*size + x]);
			}
		}
		return output;
	}

	template <class Type>
	matrix<Type>::operator matrix<double>(){
		matrix<double> output(width, height);
		for (int y = 0; y < height; y++){
			for (int x = 0; x < width; x++){
				output[y][x] = static_cast<double>(data[y*size + x]);
			}
		}
		return output;
	}

	//vector functions
	template <class Type>
	vector<Type>::vector(int in_height){
		static_assert(std::_Is_numeric<Type>::value,
			"Cannot initialise matrix with non numeric types");
		width = 1;
		height = in_height;
		size = height;
		try {
			data = new Type[size*size];
		}
		catch (std::bad_alloc){
			throw(matrixException(MEMORY_ERROR));
		}
	}

	template <class Type>
	vector<Type>::vector(int in_height, Type* in_data){
		static_assert(std::_Is_numeric<Type>::value,
			"Cannot initialise matrix with non numeric types");
		width = 1;
		height = in_height;
		size = height;
		try {
			data = new Type[size*size];
		}
		catch (std::bad_alloc){
			throw(matrixException(MEMORY_ERROR));
		}
		for (int i = 0; i < height; i++){
			data[i*size] = in_data[i];
		}
	}
}

#endif
