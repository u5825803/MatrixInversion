#include <stdlib.h>
#include <iostream>
#include <conio.h>
#include "matrix.h"
#include <stdlib.h>

void test();

int main(){
	test();
	_getch();
	return 0;
}

int increment(int x){
	return ++x;
}

void test(){
	//test constructors
	using namespace Matrix;
	using namespace std;
	try { 
		matrix <double> tmp(100000, 100000);
	}
	catch (matrixException error){
		std::cout << error.getErrorMessage();
	}
	int intdata[] = { 4, 6, 2, -4, 1, 0, -4, 5, 6, -7, 3, 1, 2, -3, 5, -6, 7, 0, 3, 1, 5, 6, 4, 5 };
	float floatdata[] = { 4.4, 6.6, 2.2, -4.4, 1.1, 0.4, -4.4, 5.5, 6.6, -7.7, 3.3, 1.1, 2.2, -3.3, 5.5, -6.6, 7.7, 0.7, 3.3, 1.1, 5.5, 6.6, 4.4, 5.5 };
	//int
	cout << "integer constructor with data\n";
	matrix<int> conTestInt(4, 4, intdata);
	cout << conTestInt;
	cout << "\ninteger constructor without data\n";
	matrix<int> conTestInt2(4, 5);
	cout << conTestInt2;
	//float
	cout << "\nfloat constructor with data\n";
	matrix<float> conTestFloat(3, 3, floatdata);
	cout << conTestFloat;
	cout << "\nfloat constructor without data\n";
	matrix<float> conTestFloat2(4, 2);
	cout << conTestFloat2;
	//Test member functions
	cout << "\ngetWidth\n";
	cout << "width = 4?\t" << (conTestInt2.getWidth() == 4);
	cout << "\ngetHeight\n";
	cout << "height = 5>\t" << (conTestInt2.getHeight() == 5);
	cout << "\nmap increment\n";
	conTestInt.map(increment);
	cout << conTestInt;
	//Test matrix operators
	cout << "\nconTestInt + conTestInt\n";
	cout << (conTestInt + conTestInt);
	cout << "\nconTestInt - conTestInt\n";
	cout << (conTestInt - conTestInt);
	cout << "\nconTestInt * 2\n";
	cout << (conTestInt * 2);
	cout << "\nconTestInt == conTestInt\n";
	cout << (conTestInt == conTestInt) << '\n';
	cout << "\ndeterminant of conTestInt\n";
	cout << determinant(conTestInt) << '\n';
	cout << "\ntranspose of conTestInt\n";
	cout << transpose(conTestInt);
	cout << "\ncofactor matrix of conTestInt\n";
	cout << cofactor(conTestInt);
	cout << "\nadjoint of conTestInt\n";
	cout << adjoint(conTestInt);
	cout << "\nInverse of conTestInt\n";
	cout << invert(conTestInt);
	cout << "\nA*A^-1\n";
	cout << (invert(conTestInt) * static_cast<matrix<double>>(conTestInt));
}